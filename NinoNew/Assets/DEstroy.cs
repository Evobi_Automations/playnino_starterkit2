using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class DEstroy : MonoBehaviour
{
    // public bool sound;
    // AudioSource audioData;

    BeeperController beeperController;
    private void Awake()
    {
        //sound = false;
    }
    // Start is called before the first frame update
    void Start()
    {
        beeperController = FindObjectOfType<BeeperController>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider collision) {
 
        Debug.Log("Destroyed");

        Destroy(collision.transform.gameObject);
        if(collision.gameObject.GetComponent<electronBall>().beep==true)
            beeperController.BeeperSound();
    }

}
