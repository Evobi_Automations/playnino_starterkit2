﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TrackableEventHandler : DefaultTrackableEventHandler
{

	public GameObject trackableObj;
	protected override void OnTrackingFound()
	{
		base.OnTrackingFound();
		Debug.Log(base.mTrackableBehaviour.TrackableName);

		trackableObj.SetActive(true);
	}
	protected override void OnTrackingLost()
	{
		base.OnTrackingLost();
	}

	// Player.transform.position = Vector3.Lerp(startPosition, CurrentPositionHolder, Timer);
}

