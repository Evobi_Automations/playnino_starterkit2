﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Timers;
using UnityEngine;

public class eform : MonoBehaviour
{
    public GameObject player;
    public GameObject parent;
    public static int i = 0;

    [SerializeField]
    float TimerIntervalInSeconds;


    bool isTimeElapsed;
    float Timer;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        TimerInSeconds();
        //Debug.log(currentNode);
        if (isTimeElapsed)
        {
            GameObject clone = Instantiate(player, transform.position, Quaternion.identity, parent.transform);
            clone.gameObject.SetActive(true);
            isTimeElapsed = false;
            Timer = 0;
        }

       
    }

    void TimerInSeconds()
    {
        Timer = Timer + Time.deltaTime*5;
        float seconds = Timer % 60;
        if (seconds > TimerIntervalInSeconds)
        {
            isTimeElapsed = true;
        }
        else
        {
            isTimeElapsed = false;
        }
    }
}