﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class electronBall : MonoBehaviour
{


    WaypointController waypointController;
    public enum electrontype { BALL};
    public electrontype _electronType;
    public float speed;
    public float speedDecrease;
    int sizeo;
    int waypointnumber;
    public bool beep=false;
    // Start is called before the first frame update

    // Update is called once per frame

    // Start is called before the first frame update
    void Start()
    {
        waypointController = FindObjectOfType<WaypointController>();
        sizeo=waypointController.waypoints.Count;
    }

    // Update is called once per frame

        void Update()
        {

            //Debug.Log("SPEED>>>" + _electronBall.speed);
            float movementstep = speed * Time.deltaTime;
            float distance = Vector3.Distance(transform.position, waypointController.waypoints[waypointnumber].transform.position);
            CheckDistanceToWaypoint(distance);

            transform.position = Vector3.MoveTowards(transform.position, waypointController.waypoints[waypointnumber].transform.position, movementstep);
          }
        void CheckDistanceToWaypoint(float currentDistance)
        {
            if (currentDistance <=waypointController.minDistance)
            {

                //waypointnumber++;
                waypointnumber=(waypointnumber+1)%sizeo;
             //  waypointController.targetWaypointIndex++;
                UpdateTargetWaypoint();
            }
        }
        void UpdateTargetWaypoint()
        {
            if (waypointController.targetWaypointIndex > waypointController.lastWaypointIndex)
            {
                //condition to be put here for the buzzer sound;
                //  gameObject.active = false;
            }
        waypointController.targetWaypoint = waypointController.waypoints[waypointController.targetWaypointIndex];
        }


}
